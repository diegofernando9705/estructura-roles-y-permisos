@foreach($reportes as $reporte)
<tr>
<td>
    {{ $reporte->identificacion }}
</td>
<td>
    {{ $reporte->nombre }}
</td>
<td>
    {{ $reporte->fecha }}
</td>
<td>
    {{ $reporte->descripcion }}
</td>
<td>
    {{ $reporte->regional }}
</td>
<td>
    {{ $reporte->asociado }}
</td>
</tr>
@endforeach

{{ $reportes->render() }}