<div class="row">
    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">B&uacute;squeda por fechas:</b>
            <hr />
            <div class="form-group col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <input type="date" class="form-control" name="fechaInicial" id="fechaInicial">
                <em>Fecha inicial</em>
            </div>
            <div class="form-group col-6 col-md-6 col-lg-6 col-sm-6 col-xl-6">
                <input type="date" class="form-control" name="fechaFinal" id="fechaFinal">
                <em>Fecha Final</em>
            </div>
        </div>
    </div>
    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Conferencias:</b>
            <hr />
            <div class="col-lg-6 mx-auto">
            <label class="text-white mb-3 lead">Where do you live?</label>
            <!-- Multiselect dropdown -->
            <select multiple data-style="bg-white rounded-pill px-4 py-3 shadow-sm " class="selectpicker w-100">
                <option>United Kingdom</option>
                <option>United States</option>
                <option>France</option>
                <option>Germany</option>
                <option>Italy</option>
            </select><!-- End -->
        </div>
        </div>
    </div>
    <div class="fechas col-12 col-md-4 col-lg-4 col-sm-4 col-xl-4">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Asociados:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <select class="form-control mi-selector" name="asociado" id="asociado">
                    <option value="">Ambos</option>
                    <option value="1">Asociado</option>
                    <option value="0">No asociado</option>
                </select>
            </div>
        </div>
    </div>
    <div class="fechas col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <div class="row">
            <b class="col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">Regionales:</b>
            <hr />
            <div class="form-group col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
                <select class="form-control mi-selector" name="regional" id="regional">
                    @foreach($regionales as $regional)
                    <option value="{{ $regional->id }}">{{ $regional->nombre }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <hr>
    <div class="tabla  col-12 col-md-12 col-lg-12 col-sm-12 col-xl-12">
        <table class="table table-bordered table-hover">
            <thead>
            <th>
                Identificación
            </th>
            <th>
                Nombre
            </th>
            <th>
                Fecha
            </th>
            <th>
                Mensaje
            </th>
            <th>
                Regional
            </th>
            <th>
                Asociado
            </th>
            </thead>
            <tbody id="resultadoConsulta">

            </tbody>
        </table>

    </div>
</div>
<button type="button" class="btn btn-info btn-buscar">Buscar</button>