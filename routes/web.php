<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Permisos\Models\Role;
use App\Permisos\Models\Permission;
use Illuminate\Support\Facades\Gate;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('/role', 'RoleController')->names('role');
Route::resource('/reportes', 'ReportesController')->names('reportes');
Route::get('/reportes/seleccion/{formulario}', 'ReportesController@formulario');
Route::post('/reportes/generate', 'ReportesController@generate')->name('reportes.generate');

Route::resource('/user', 'UserController', ['except' => [
        'create', 'store']])->names('user');
