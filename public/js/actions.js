$(document).ready(function () {

$(function () {
    $('.selectpicker').selectpicker();
});

    $(document).on("change", ".tablaSeleccionada", function () {
        var valor = $(this).val();

        $.ajax({
            url: '/reportes/seleccion/' + valor,
            type: 'get',
            success: function (data) {
                $("#camposReportes").html(data);
            }
        });
    });


    $(document).on("click", ".btn-buscar", function () {

        var url = $("#formTodo").attr('data-action');
        ///alert($('#fechaInicial').val());
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                '_token': $('input[name=_token]').val(),
                'tabla': $('#tabla').val(),
                'fechaInicial': $('#fechaInicial').val(),
                'fechaFinal': $('#fechaFinal').val(),
                'conferencia': $('#conferencia').val(),
                'asociado': $('#asociado').val(),
                'regional': $('#regional').val()
            },
            success: function (data) {
                //  alert(data);
                $("#resultadoConsulta").html(data);
            }
        });
    });

});
;