# Estructura de Roles y Permisos para Reportes


Este es un proyecto en Laravel 7, el cual es un sistema de Roles y Permisos.

Para poder utilizar este proyecto, debes tener los siguientes requisitos:

a) debes tener la versión de PHP mayor o igual a la 7
para mas información visita la documentación oficial de Laravel: https://laravel.com/docs/7.x

b) debes tener instalado composer en tu equipo: https://getcomposer.org/

c) Si se utiliza windows, descargue el programa git desde la página oficial: https://gitforwindows.org/