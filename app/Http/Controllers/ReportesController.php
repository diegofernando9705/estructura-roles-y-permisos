<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReportesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('reportes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request) {

        if ($request->tabla == 'todo') {

            if (empty($request->fechaInicial)) {
                $fechaInicial = '';
            } else {
                $fechaInicial = $request->fechaInicial;
            }

            if (empty($request->fechaFinal)) {
                $fechaInicial = '';
            } else {
                $fechaInicial = $request->fechaInicial;
            }

            $reportes = DB::table('accesos')
                            ->join('accesos_historial', 'accesos.identificacion', '=', 'accesos_historial.identificacion')
                            ->join('mensajes', 'accesos.identificacion', '=', 'mensajes.identificacion')
                            ->where('accesos.asociado', $request->asociado)
                            ->whereBetween('accesos_historial.fecha', [$request->fechaInicial, $request->fechaFinal])->paginate();


            return view('reportes.formularios.respuesta.todo', compact('reportes'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function formulario($form) {
        //return $form;
        $regionales = DB::table('regionales')->get();

        if ($form == 'todo') {
            return view('reportes.formularios.todo', compact('regionales'));
        }
    }

}
